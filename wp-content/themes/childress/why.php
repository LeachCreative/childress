<?php
	/*
		Template Name: Why
	*/

	get_header();
?>
<?php $img =  get_field('main_image'); ?>
<section class="slider clearfix" style="background:url('<?php echo $img['url']; ?>');background-size:100%">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
				
					<div class="slider-box">
						<div class="col-md-7">
							<h1><?php echo get_field('main_image_message'); ?></h1>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</section>
	<section class="three-call">
		<div class="container">
			<div class="row">
				<?php

					// check if the repeater field has rows of data
					if( have_rows('3_column_panel') ):
					 	// loop through the rows of data
					    while ( have_rows('3_column_panel') ) : the_row();
							$image = get_sub_field('image');
							$title = get_sub_field('title');
							$content = get_sub_field('content');
							$color = get_sub_field('bg_color');
							$link = get_sub_field('link_to_row');
					        echo '<div class="col-md-4 col-sm-4 col-xs-12">';
					        	/*echo '<div class="img-wrap">';
				        			echo '<div>';
					        			echo '<img src="'. $image['url'] . '" alt="' . $image['alt'] . '">'	;
					        			echo '<div class="overlay"></div>';
					        		echo '</div>';
					        			echo '<div class="center title">'. $title . '</div>';

					        	echo '</div>';
					        	*/
					        	 echo '<a href="#row'. $link . '">
						            <div class="img-overlay">
						                <img src="' . $image['url'] .'" class="img-responsive"/>
						                <div class="fa fa-plus project-overlay"></div>
						                <div class="center title">'. $title . '</div>
						            </div>
						            
						        </a>
					        </div>'; // col-md-4
					    endwhile;

					else :

					    // no rows found

					endif;

				?>
			</div>
		</div>
	</section>
	<section class="flexible-content">
		<div class="container">
			
				<?php

					// check if the flexible content field has rows of data
					if( have_rows('panel_3') ):
						$count = 1;
					     // loop through the rows of data
					    while ( have_rows('panel_3') ) : the_row();

					        if( get_row_layout() == 'image_left' ):
					        	echo '<div id="row' . $count . '" class="row">';
					        	echo '<div class="col-md-4 col-xs-12">';
					        		$img = get_sub_field('image');
					        		echo '<img src="' . $img['url'] . '">';
					        	echo '</div>';
					        	echo '<div class="col-md-8 col-xs-12">';
					        		echo '<h1>' . get_sub_field('title') . '</h1>';
					        		echo get_sub_field('content');
					        	echo '</div>';
					        	echo '</div>';
					        elseif( get_row_layout() == 'image_right' ): 
					        	echo '<div id="row' . $count . '" class="row">';
					        	echo '<div class="col-md-4 col-md-push-8 col-xs-12">';
					        		$img = get_sub_field('image');
					        		echo '<img src="' . $img['url'] . '">';
					        	echo '</div>';
					        	echo '<div class="col-md-8 col-md-pull-4 col-xs-12">';
					        		echo '<h1>' . get_sub_field('title') . '</h1>';
					        		echo get_sub_field('content');
					        	echo '</div>';
					        	echo '</div>';
					        elseif( get_row_layout() == 'quote_left' ): 
					        	echo '<div id="row' . $count . '" class="row">';
					        	echo '<div class="col-md-4 col-xs-12">';
					        		echo '<blockquote>' . get_sub_field('quote') . '</blockquote>';
					        	echo '</div>';
					        	echo '<div class="col-md-8 col-xs-12">';
					        		echo '<h1>' . get_sub_field('title') . '</h1>';
					        		echo get_sub_field('content');
					        	echo '</div>';
					        	echo '</div>';
					        elseif( get_row_layout() == 'quote_right' ): 
					        	echo '<div id="row' . $count . '" class="row">';
					        	echo '<div class="col-md-8 col-xs-12">';
					        		echo '<h1>' . get_sub_field('title') . '</h1>';
					        		echo get_sub_field('content');
					        	echo '</div>';
					        	echo '<div class="col-md-4 col-xs-12">';
					        		echo '<blockquote>' . get_sub_field('quote') . '</blockquote>';
					        	echo '</div>';
					        	echo '</div>';
					        endif;
					        $count++;

					        echo '<div class="row">';
					        echo '<div class="col-md-12"><div class="divider"></div></div><div class="col-md-1"></div>';
					        echo '</div>';
					    endwhile;

					else :

					    // no layouts found

					endif;

				?>
		</div>
	</section>
	
	<?php get_template_part('mailing'); ?>

<?php get_footer(); ?>