<?php
	/*
		Template Name: About
	*/

	get_header();
?>
<?php $img =  get_field('main_image'); ?>
<section class="slider clearfix" style="background:url('<?php echo $img['url']; ?>');background-size:100%">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
				
					<div class="slider-box">
						<div class="col-md-7">
							<h1><?php echo get_field('main_image_message'); ?></h1>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</section>
	<section class="three-call">
		<div class="container">
			<div class="row">
				<?php

					// check if the repeater field has rows of data
					if( have_rows('3_column_panel') ):

					 	// loop through the rows of data
					    while ( have_rows('3_column_panel') ) : the_row();
							$image = get_sub_field('image');
							$title = get_sub_field('title');
							$content = get_sub_field('content');
							$color = get_sub_field('bg_color');
							$link = get_sub_field('page_link');
					        echo '<div class="col-md-4 col-sm-4 col-xs-12">';
					        	/*echo '<div class="img-wrap">';
				        			echo '<div>';
					        			echo '<img src="'. $image['url'] . '" alt="' . $image['alt'] . '">'	;
					        			echo '<div class="overlay"></div>';
					        		echo '</div>';
					        			echo '<div class="center title">'. $title . '</div>';

					        	echo '</div>';
					        	*/
					        	 echo '<a href="' . $link .'">
						            <div class="img-overlay">
						                <img src="' . $image['url'] .'" class="img-responsive"/>
						                <div class="fa fa-plus project-overlay"></div>
						                <div class="center title">'. $title . '</div>
						            </div>
						            
						        </a>
					        </div>'; // col-md-4

					    endwhile;

					else :

					    // no rows found

					endif;

				?>
			</div>
		</div>
	</section>
	<section class="in-the-news">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="divider"></div>
					<h1><?php the_field('panel_2_headline'); ?></h1>
				</div>
			</div>
			<div class="row">

				<?php 

					$args = array(
						'post_type' => 'inthenews',
						'posts_per_page' => 16
					);
					$custom_query = new WP_Query($args);
					while($custom_query->have_posts()) : $custom_query->the_post(); 

						echo '<div class="col-md-3">';
							echo '<a href="' . get_field('link') . '">';
							the_post_thumbnail();
							echo '</a>';
							echo '<p>' . get_field('news_story_date') . '</p>'; 
						echo '</div>';

					endwhile;
					wp_reset_postdata();
				?>

				
			</div>
		</div>
	</section>
	<?php get_template_part('mailing'); ?>

<?php get_footer(); ?>