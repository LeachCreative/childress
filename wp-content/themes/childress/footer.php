	<div id="push"></div><!--Push -->
	</div><!-- Wrap -->
	<footer id="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-4 hidden-sm hidden-xs">
					<img src="<?php bloginfo('template_directory'); ?>/images/footer-logo.png">
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6">
					<strong>Resources</strong>
					<ul>
						<li><a href="<?php echo esc_url( get_permalink( get_page_by_title( 'About' ) ) ); ?>">About Childhood Trauma</a></li>
						<li><a href="<?php0// echo esc_url( get_permalink( get_page_by_title( 'Monthly Events' ) ) ); ?>">Annual Report</a></li>
						<li><a href="<?php echo esc_url( get_permalink( get_page_by_title( 'Press Room' ) ) ); ?>">Press Room</a></li>
					</ul>
				</div>
				<div class="col-md-2 col-sm-3 col-xs-6">
					<strong>Get Involved</strong>
					<ul>
						<li><a href="" data-toggle="modal" data-target="#contact">Contact Us</a></li>
						<li><a href="<?php echo esc_url( get_permalink( get_page_by_title( 'Give' ) ) ); ?>">Donate</a></li>
						<li><a href="">Volunteer</a></li>
					</ul>
				</div>
				<div class="col-md-2 col-sm-3 col-xs-6">
					<strong>About Us</strong>
					<ul>
						<li></li>
						<li><a href="<?php echo esc_url( get_permalink( get_page_by_title( 'Mission and Values' ) ) ); ?>">Mission and Values</a></li>
						<li><a href="<?php echo esc_url( get_permalink( get_page_by_title( 'History' ) ) ); ?>">History</a></li>
						<li><a href="<?php echo esc_url( get_permalink( get_page_by_title( 'Staff' ) ) ); ?>">Staff</a></li>
					</ul>
				</div>
				<div class="col-md-2 col-sm-3 col-xs-6">
					<strong>Connect</strong>
					<ul>
						<li><a target="_blank" href="https://www.facebook.com/ChildressPediatricTrauma">Facebook</a></li>
						<li><a target="_blank" href="https://twitter.com/injuredkids">Twitter</a></li>
						<li><a target="_blank" href="https://www.youtube.com/channel/UC5tEnyrmhbnOBNxyeFt79Lw">YouTube</a></li>
					</ul>
				</div>
				
			</div>
		</div>
	</footer>
	<?php wp_footer(); ?>

	<!-- Modal -->
	<div class="modal fade" id="contact" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	        <h4 class="modal-title" id="myModalLabel">Thank You</h4>
	      <div class="modal-body" style="display:block !important">
	        <?php echo do_shortcode('[contact-form-7 id="311" title="Contact Us"]'); ?>
	        <style>
	        #gform_wrapper_1 {
	        	display:block !important;
	        }
	        </style>
	      </div>
	      <?php /*<div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary">Save changes</button>
	      </div>*/ ?>
	    </div>
	  </div>
	</div>

	
  </body>
</html>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-56799656-1', 'auto');
  ga('send', 'pageview');
</script>