<?php
	/*
		Template Name: How
	*/
	if($_SERVER['SERVER_PORT'] == '443')
	{
		header('Location: http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
	}
	get_header();
?>
<?php $img =  get_field('main_image'); ?>
<section class="slider clearfix" style="background:url('<?php echo $img['url']; ?>');background-size:100%">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
				
					<div class="slider-box">
						<div class="col-md-7">
							<h1><?php echo get_field('main_image_message'); ?></h1>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</section>
	<section class="three-call">
		<div class="container">
			<div class="row">
				<?php

					// check if the repeater field has rows of data
					if( have_rows('3_column_panel') ):

					 	// loop through the rows of data
					    while ( have_rows('3_column_panel') ) : the_row();
							$image = get_sub_field('image');
							$title = get_sub_field('title');
							$content = get_sub_field('content');
							$color = get_sub_field('bg_color');
							$link = get_sub_field('page_link');
					        echo '<div class="col-md-4 col-sm-4 col-xs-12">';
					        	/*echo '<div class="img-wrap">';
				        			echo '<div>';
					        			echo '<img src="'. $image['url'] . '" alt="' . $image['alt'] . '">'	;
					        			echo '<div class="overlay"></div>';
					        		echo '</div>';
					        			echo '<div class="center title">'. $title . '</div>';

					        	echo '</div>';
					        	*/
					        	 echo '<a href="' . $link .'">
						            <div class="img-overlay">
						                <img src="' . $image['url'] .'" class="img-responsive"/>
						                <div class="fa fa-plus project-overlay"></div>
						                <div class="center title">'. $title . '</div>
						            </div>
						            
						        </a>
						       
					        </div>'; // col-md-4

					    endwhile;

					else :

					    // no rows found

					endif;

				?>
			</div>
		</div>
	</section>

	<section class="title-image">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="divider"></div>
					<h1><?php the_field('panel_2_headline'); ?></h1>
					<?php 
						$image = get_field('panel_2_image'); 
						echo '<img src="' . $image['url'] . '" alt="' . $image['alt'] .'">';
					?>
				</div>
			</div>
		</div>
	</section>
	<section class="title-indented-copy">
		<div class="container">
			<div class="col-md-12">
				<div class="divider"></div>
				<h1><?php the_field('panel_3_headline'); ?></h1>
			</div>
			<div class="row">
				<div class="col-md-8 col-md-push-2">
					<?php the_field('panel_3_copy'); ?>
				</div>
			</div>
		</div>
	</section>
	<?php get_template_part('mailing'); ?>

<?php get_footer(); ?>