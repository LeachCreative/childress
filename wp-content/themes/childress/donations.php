<?php
	/*
		Template Name: Donations
	*/
	/*if($_SERVER['SERVER_PORT'] != '443')
	{
		header('Location: https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
	}*/


		if ($_POST) {
	   $error = NULL;
	    //var_dump($_POST);
	    try {

	    	if(isset($_POST['custom_donation'])) {
		        $amount = strip_tags(mysql_real_escape_string((double)$_POST['custom_donation'])) * 100;

		        //echo '<h2>' . $amount . '</h2>';
		    }

	        if (isset($_POST['customer_id'])) {
		        $charge = Stripe_Charge::create(array(
		        'customer'    => $_POST['customer_id'],
		        'amount'      => $amount,
		        'currency'    => 'usd',
		        'description' => 'Donation'));
	        }
	        elseif (isset($_POST['stripeToken'])) {
	        	
	        	// Simple uniqueness check on the email address
	        	$token = $_POST['stripeToken'];
	        	$email = $_POST['stripeEmail'];
	        	
	        	// args
				$args = array(
					'numberposts' => -1,
					'post_type' => 'donations',
					'meta_key' => 'email',
					'meta_value' => $email
				);

				// get results
				$my_query = new WP_Query( $args );


				if( $my_query->have_posts()) {
			        //if $my_query finds anything loop through it here
			        while($my_query->have_posts()) : $my_query->the_post();
			        $customer_id = get_field('customer_id');
			        if($customer_id != ''):
			        	break;
			        endif;
			        endwhile;
			        wp_reset_postdata();
			    } else {
			        //if $my_query does not find anything
			        echo $customer_id;
			        // create a new customer if our current user doesn't have one
	                $customer = Stripe_Customer::create(array(
	                        'card' => $token,
	                        'email' => $email
	                    )
	                );
	                $customer_id = $customer->id;           
			    }

			    // Checks to make sure we have a customer ID
                if( $customer_id ) {

                    $charge = Stripe_Charge::create(array(
                            'amount' => $amount, // amount in cents
                            'currency' => 'usd',
                            'customer' => $customer_id
                        )
                    );

                } else {
                    // the customer wasn't found or created, throw an error
                    throw new Exception( __( 'A customer could not be created, or no customer was found.', 'pippin' ) );
                }

	      	}
		    else {
		        throw new Exception("The Stripe Token or customer was not generated correctly");
		    }
	    } catch (Exception $e) { // End Try
	      $error = $e->getMessage();
	    }

	    if ($error == NULL) {
	        // Create Post
			$my_post = array(
			  'post_title'    => 'Donation By: '. $_POST['stripeEmail'],
			  'post_type'     => 'donations',
			  'post_status'   => 'private',
			  'post_author'   => 1,
			);

			// Insert the post into the database
			$post_id = wp_insert_post( $my_post );
			
			update_field('field_545e4adb23773', strip_tags(mysql_real_escape_string($_POST['stripeEmail'])), $post_id); // Email
			update_field('field_545e4ae5d6266', '$' . strip_tags(mysql_real_escape_string($_POST['custom_donation'])), $post_id); // Amount
			//update_field('field_545e4aecd6267', strip_tags(mysql_real_escape_string($_POST['honor'])), $post_id); // Honor Of
			update_field('field_545e4dfac41b4', $customer_id, $post_id); //Customer ID
	    }
	    else {
	      echo "<div class=\"error\">".$error."</div>";
	      //require_once('./payment_form.php');
	    }
	}

	get_header();
?>
<?php $img =  get_field('background_image'); ?>
<style>
	body {
		background:url('<?php echo $img['url']; ?>') top center fixed;
	}
</style>
<section class="page-title">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Save injured kids. Make a gift.</h1>
			</div>
		</div>
	</div>
</section>
<section class="donation">
	<div class="container">
		<div class="col-md-6 col-md-push-3">
			<form id="donation" action="<?php echo esc_url( get_permalink() ); ?>" method="post">
				<h3>Donate Now.</h3>
				<input type="hidden" name="single_donation" value="single_donation" />
				<input type="text" id="custom-donation" name="custom_donation" value="30.00" /><br>
			    <script src="https://button.stripe.com/v1/button.js" 
			    	class="stripe-button"
			      data-key="pk_test_dH4gBtfiuvbiSAsY8OFQbXWs"
			      data-description="Donation"
			      data-email=""
			      data-zipcode=true
			      data-allow-remember-me=false
			      ></script>
			      <style>
			      form button {
			      	text-indent:-9999;
			      }
			      .stripe-button-el {
						background-image:none !important;
						background-image:url('<?php bloginfo('template_directory'); ?>/images/stripe-btn.png') !important;
						background:transparent !important;
						box-shadow:none !important;
						-webkit-box-shadow:none !important;
						
					}
					.stripe-button-el span {
						background:transparent;
						background-image:url('<?php bloginfo('template_directory'); ?>/images/stripe-btn.png') !important;
						width:239px;
						height:58px;
						text-indent:-9999;	
					}
					</style>
					<script>
						jQuery('.stripe-button-el span').empty();
					</script>
					<!-- Button trigger modal -->
					<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#alternative-donations">
					  Learn about other <br class="res-break">ways to give.
					</button>
			</form>
		</div>
	</div>
</section>
	

<?php get_footer(); ?>
<?php
	/*<script type="text/javascript">
	jQuery('#custom-donation').change(function(){
	      // Stripe accepts payment amounts in cents so we have to convert dollars to cents by multiplying by 100
	     var amount = parseInt( jQuery(this).val()*100);
	     jQuery(".stripe-button").attr( "data-amount", amount );
	});
	</script>*/
	if($_POST):
		if($error == NULL): ?>
			<script type="text/javascript">
			    jQuery(window).load(function($){
			        jQuery("#success").modal("show");
			    });
			</script>
		<?php
		endif;
	endif;

?>
	
<!-- Modal -->
<div class="modal fade" id="success" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Thank You</h4>
      <div class="modal-body">
        <?php echo get_field('success_message'); ?>
      </div>
      <?php /*<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>*/ ?>
    </div>
  </div>
</div>
<div class="modal fade" id="alternative-donations" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Other Ways to Give:</h4>
      <div class="modal-body">
        <?php echo get_field('alternative_modal'); ?>
      </div>
      <?php /*<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>*/ ?>
    </div>
  </div>
</div>