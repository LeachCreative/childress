<?php
	/*
		Template Name: Page
	*/
	get_header();
	$id = get_queried_object_id();
?>
	<?php require_once('inc/page-title.php'); ?>
	<section class="list">
		<div class="container">
			<div class="row">
				<div class="col-md-10 col-md-push-1"><div class="divider"></div></div><div class="col-md-1"></div>
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<div class="col-md-10 col-md-push-1 content">
					<?php if(get_field('video') != ''): ?>
						<div class="embed-container">
							<?php echo the_field('video'); ?>
						</div>
					<?php endif; ?>
					<?php the_content(); ?>
				</div>
				<?php endwhile; else : ?>
					<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
				<?php endif; ?>
			</div>
		</div>
	</section>
	
	<?php get_template_part('mailing'); ?>

<?php get_footer(); ?>



