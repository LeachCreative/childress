<section class="mailing">
	<div class="container">
		<div class="row">
			<div class="subscribe">
				<div class="col-md-3">
					<p>Join our mailing list.</p>
				</div>
				<form action="//injuredkids.us9.list-manage.com/subscribe/post?u=eac15117da2537379341a6579&amp;id=11e22b4f08" method="post" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
					<div class="col-md-3">
						<input type="text" name="FNAME" placeholder="First Name">
					</div>
					<div class="col-md-3">
						<input type="text" name="LNAME" placeholder="Last Name">
					</div>
					<div class="col-md-3 submit-button-column">
						<input type="text" name="EMAIL" placeholder="Email Address">
						<button type="submit"><img src="<?php bloginfo('template_directory'); ?>/images/arrow.png"></button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>