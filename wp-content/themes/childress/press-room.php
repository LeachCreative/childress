<?php
	/*
		Template Name: Press Room
	*/

	get_header();
?>
	<?php require_once('inc/page-title.php'); ?>
	
	<section class="in-the-news">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="divider"></div>
				</div>
			</div>
			<div class="row">

				<?php 

					$args = array(
						'post_type' => 'inthenews',
						'posts_per_page' => -1,
						'meta_key'		=> 'news_story_date',
						'orderby'		=> 'meta_value_num',
					);
					$custom_query = new WP_Query($args);
					while($custom_query->have_posts()) : $custom_query->the_post(); 

						echo '<div class="col-md-3">';
							echo '<a href="' . get_field('link') . '">';
							the_post_thumbnail();
							echo '</a>';
							echo '<p>' . get_field('news_story_date') . '</p>'; 
						echo '</div>';

					endwhile;
					wp_reset_postdata();
				?>

				
			</div>
		</div>
	</section>
	<?php get_template_part('mailing'); ?>

<?php get_footer(); ?>