<?php
	/*
		Template Name: Staff
	*/

	get_header();
?>
	<?php require_once('inc/page-title.php'); ?>
	<section class="list">
		<div class="container">
			
				<?php

					// check if the repeater field has rows of data
					if( have_rows('staff') ):

					 	// loop through the rows of data
					    while ( have_rows('staff') ) : the_row();
							$image = get_sub_field('staff_photo');
							$name = get_sub_field('staff_name');
							$title = get_sub_field('staff_title');
							$bio = get_sub_field('staff_bio');
					        echo '<div class="row">';
					        	echo '<div class="col-md-10 col-md-push-1"><div class="divider"></div></div><div class="col-md-1"></div>';
					        	echo '<div class="col-md-2 col-md-push-1"><div class="wrapper"><img src="'. $image['url'] . '" alt="' . $image['alt'] . '"></div></div>';
					        	echo '<div class="col-md-8 col-md-push-1"><div class="wrapper"><h2>' . $name .'</h2><p><strong>'. $title .'</strong><p>' . $bio . '</p></div></div>';
					        echo '</div>'; // col-md-4

					    endwhile;

					else :

					    // no rows found

					endif;

				?>
		</div>
	</section>
	
	<?php get_template_part('mailing'); ?>

<?php get_footer(); ?>