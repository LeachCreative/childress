<?php get_header(); ?>

	<?php require_once('inc/page-title.php'); ?>
	<section class="list">
		<div class="container">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<div class="row">
					<div class="col-md-10 col-md-push-1"><div class="divider"></div></div><div class="col-md-1"></div>
					<div class="col-md-10 col-md-push-1 content">
						<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						<?php the_excerpt(); ?><a class="readmore" href="<?php the_permalink(); ?>">Read More. ></a>
					</div>
				</div>
			<?php endwhile; else : ?>
				<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>
			<div class="pagination">
				<?php echo paginate_links( $args ); ?>
			</div>
		</div>
	</section>
	
	<?php get_template_part('mailing'); ?>

<?php get_footer(); ?>
