<?php get_header(); ?>

	<?php require_once('inc/page-title.php'); ?>
	<section class="list">
		<div class="container">
				<div class="row">
					<?php

						// check if the repeater field has rows of data
						if( have_rows('404', 'option') ):

						 	// loop through the rows of data
						    while ( have_rows('404', 'option') ) : the_row();
								//$image = get_sub_field('image');
								$title = get_sub_field('title', 'option');
								$content = get_sub_field('content', 'option');
								$icon = get_sub_field('icon', 'option');
						        echo '<div class="row">';
						        	echo '<div class="col-md-10 col-md-push-1"><div class="divider"></div></div><div class="col-md-1"></div>';
						        	echo '<div class="col-md-2 col-md-push-1"><div class="wrapper"><img src="'. $icon['url'] . '" alt="' . $icon['alt'] . '"></div></div>';
						        	echo '<div class="col-md-8 col-md-push-1"><div class="wrapper"><h2>' . $title .'</h2><p>' . $content . '</p></div></div>';
						        echo '</div>'; // col-md-4

						    endwhile;

						else :

						    // no rows found

						endif;

					?>
					
			</div>
		</div>
	</section>
	
	<?php get_template_part('mailing'); ?>

<?php get_footer(); ?>