<?php
	/*
		Template Name: Help
	*/

	get_header();
?>
<?php $img =  get_field('main_image'); ?>
<section class="slider clearfix" style="background:url('<?php echo $img['url']; ?>');background-size:100%">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
				
					<div class="slider-box">
						<div class="col-md-7">
							<h1><?php echo get_field('main_image_message'); ?></h1>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</section>
	<?php require_once('inc/page-title.php'); ?>
	<section class="list">
		<div class="container">
			
				<?php

					// check if the repeater field has rows of data
					if( have_rows('help') ):

					 	// loop through the rows of data
					    while ( have_rows('help') ) : the_row();
							//$image = get_sub_field('image');
							$title = get_sub_field('title');
							$content = get_sub_field('content');
							$icon = get_sub_field('icon');
					        echo '<div class="row">';
					        	echo '<div class="col-md-10 col-md-push-1"><div class="divider"></div></div><div class="col-md-1"></div>';
					        	echo '<div class="col-md-2 col-md-push-1"><div class="wrapper"><img src="'. $icon['url'] . '" alt="' . $icon['alt'] . '"></div></div>';
					        	echo '<div class="col-md-7 col-md-push-1"><div class="wrapper"><h2>' . $title .'</h2><p>' . $content . '</p></div></div>';
					        echo '</div>'; // col-md-4

					    endwhile;

					else :

					    // no rows found

					endif;

				?>
		</div>
	</section>
	
	<?php get_template_part('mailing'); ?>

<?php get_footer(); ?>