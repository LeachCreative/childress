<?php
	function add_slug_class_to_menu_item($output){
		$ps = get_option('permalink_structure');
		if(!empty($ps)){
			$idstr = preg_match_all('/<li id="menu-item-(\d+)/', $output, $matches);
			foreach($matches[1] as $mid){
				$id = get_post_meta($mid, '_menu_item_object_id', true);
				$slug = basename(get_permalink($id));
				$output = preg_replace('/menu-item-'.$mid.'">/', 'menu-item-'.$mid.' menu-item-'.$slug.'">', $output, 1);
			}
		}
		return $output;
	}
	add_filter('wp_nav_menu', 'add_slug_class_to_menu_item');

	//Includes
	$includes = array(
		'navwalker' => 'inc/wp_bootstrap_navwalker.php',
		'ACF'       => 'acf_include.php',
		'Stripe'    => 'stripe-php/lib/Stripe.php'
	);
	foreach($includes as $key => $value) {
		// Locate Include
		$located = locate_template($value);
		if ( !empty($located)) {
			require_once($located);
		}
	}



	//Enqueue
	function theme_name_scripts() {
	
		wp_enqueue_style( 'normalize', get_template_directory_uri() . '/css/normalize.css', array(), '1.0.0', false );
		//wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '1.0.0', false );
		wp_enqueue_style( 'layout', get_template_directory_uri() . '/style.css', array(), '1.0.0', false );

		//wp_enqueue_script('jquery-1', get_template_directory_uri() . '/scripts/jquery.min.js', array(), '1.0.0', false );
		wp_enqueue_script('jquery');
		wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '1.0.0', false );
		
	}

	add_action( 'wp_enqueue_scripts', 'theme_name_scripts' );


	add_theme_support('menus' );
	
	Stripe::setApiKey('sk_test_tmsGVmjJs7euNIRPhYRZB1L4');
	

	if( function_exists('acf_add_options_sub_page') )
	{
	    acf_add_options_sub_page(array(
	        'title' => 'Options',
	        'parent' => 'options-general.php',
	        'capability' => 'manage_options'
	    ));
	}

	
?>