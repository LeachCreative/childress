<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">

			<title>Childress</title>
		
		<link href='http://fonts.googleapis.com/css?family=Rokkitt:400,700|Raleway:400,500,600,300,200,700,800,900' rel='stylesheet' type='text/css'>

		<!-- Just for debugging purposes. Don't actually copy this line! -->
		<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
	<div id="wrap">
	<nav class="navbar navbar-default navbar-static-top" role="navigation">
      <div class="container">
      	<div class="col-md-12">
      		<div class="col-md-3 col-xs-12">
		        <div class="navbar-header">
		          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
		            <span class="sr-only">Toggle navigation</span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		          </button>
		          <a class="navbar-brand page-scroll" href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/logo.png"></a>
		        </div>
	        </div>
	        <div id="navbar" class="navbar-collapse collapse">
	        <div class="col-md-6 col-xs-12">
	          <?php
		            wp_nav_menu( array(
		                'menu'              => 'Primary Navigation',
		                'theme_location'    => 'primary',
		                'depth'             => 2,
		                'container'         => 'div',
		                'container_class'   => '',
		        		'container_id'      => 'bs-example-navbar-collapse-1',
		                'menu_class'        => 'nav nav-justified nav-center',
		                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
		                'walker'            => new wp_bootstrap_navwalker())
		            );
		        ?>
		      </div>
		      <div class="col-md-3 navbar-nav-right">
		          <div class="nav navbar-nav navbar-right">
		          	<div class="nav-social">
		          		<ul>
		          			<li class="twitter"><a target="_blank" href="https://twitter.com/injuredkids"><img src="<?php bloginfo('template_directory'); ?>/images/twitter.png"></a></li>
		          			<li class="facebook"><a target="_blank" href="https://www.facebook.com/ChildressPediatricTrauma"><img src="<?php bloginfo('template_directory'); ?>/images/facebook.png"></a></li>
		          			<li class="youtube"><a target="_blank" href="https://www.youtube.com/user/ChildressInstitute"><img src="<?php bloginfo('template_directory'); ?>/images/youtube.png"></a></li>
		          		</ul>
		          	</div>
		            <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
						<label>
							<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
						</label>
						<button type="submit"><img src="<?php bloginfo('template_directory'); ?>/images/search.png"></button>
					</form>
		          </div>
		        </div>
	        </div><!--/.nav-collapse -->
	      </div><!-- col-md-12-->	
      </div>
    </nav>