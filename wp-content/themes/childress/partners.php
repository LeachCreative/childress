<?php
	/*
		Template Name: Partners
	*/

	get_header();
?>
<?php require_once('inc/page-title.php'); ?>
<section class="in-the-news">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="divider"></div>
				<h2><?php the_field('partners_title'); ?></h2>
			</div>
		</div>
		<div class="row">

			<?php 

				$args = array(
					'post_type' => 'partners',
					'posts_per_page' => 16
				);
				$custom_query = new WP_Query($args);
				while($custom_query->have_posts()) : $custom_query->the_post(); 

					echo '<div class="col-md-3">';
						echo '<a href="' . get_field('link') . '">';
						the_post_thumbnail();
						echo '</a>';
						echo '<p>' . get_field('news_story_date') . '</p>'; 
					echo '</div>';

				endwhile;
				wp_reset_postdata();
			?>

			
		</div>
	</div>
</section>
<?php get_template_part('mailing'); ?>

<?php get_footer(); ?>