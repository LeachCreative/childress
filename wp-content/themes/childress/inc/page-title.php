<?php if(is_search()): ?>
<section class="page-title">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Search</h1>
			</div>
		</div>
	</div>
</section>
<?php elseif(is_404()) : ?>
<section class="page-title">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Sorry. The page you were looking<br> for wasn't found.</h1>
			</div>
		</div>
	</div>
</section>
<?php elseif(is_post_type_archive('blog')): ?>
	<section class="page-title">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Blog</h1>
			</div>
		</div>
	</div>
</section>
<?php elseif ( is_singular( 'blog' ) ): ?>
	<section class="page-title left">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-push-1">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</section>
<?php else : ?>
<section class="page-title">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>