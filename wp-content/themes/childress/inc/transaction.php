<?php  
	if ($_POST) {
	   $error = NULL;
	    //var_dump($_POST);
	    try {

	    	if(isset($_POST['custom_donation'])) {
		        $amount = strip_tags(mysql_real_escape_string((double)$_POST['custom_donation'])) * 100;

		        //echo '<h2>' . $amount . '</h2>';
		    }

	        if (isset($_POST['customer_id'])) {
		        $charge = Stripe_Charge::create(array(
		        'customer'    => $_POST['customer_id'],
		        'amount'      => $amount,
		        'currency'    => 'usd',
		        'description' => 'Donation'));
	        }
	        elseif (isset($_POST['stripeToken'])) {
	        	// Simple uniqueness check on the email address
	        	$token = $_POST['stripeToken'];
	        	$email = $_POST['stripeEmail'];
	        	
	        	// args
				$args = array(
					'numberposts' => -1,
					'post_type' => 'donations',
					'meta_key' => 'email',
					'meta_value' => $email
				);

				// get results
				$my_query = new WP_Query( $args );


				if( $my_query->have_posts()) {
			        //if $my_query finds anything loop through it here
			        while($my_query->have_posts()) : $my_query->the_post();
			        $customer_id = get_field('customer_id');
			        if($customer_id != ''):
			        	break;
			        endif;
			        endwhile;
			        wp_reset_postdata();
			    } else {
			        //if $my_query does not find anything
			        echo $customer_id;
			        // create a new customer if our current user doesn't have one
	                $customer = Stripe_Customer::create(array(
	                        'card' => $token,
	                        'email' => $email
	                    )
	                );
	                $customer_id = $customer->id;           
			    }

			    // Checks to make sure we have a customer ID
                if( $customer_id ) {

                    $charge = Stripe_Charge::create(array(
                            'amount' => $amount, // amount in cents
                            'currency' => 'usd',
                            'customer' => $customer_id
                        )
                    );

                } else {
                    // the customer wasn't found or created, throw an error
                    throw new Exception( __( 'A customer could not be created, or no customer was found.', 'pippin' ) );
                }

	      	}
		    else {
		        throw new Exception("The Stripe Token or customer was not generated correctly");
		    }
	    } catch (Exception $e) { // End Try
	      $error = $e->getMessage();
	    }

	    if ($error == NULL) {
	        // Create Post
			$my_post = array(
			  'post_title'    => 'Donation By: '. $_POST['stripeEmail'],
			  'post_type'     => 'donations',
			  'post_status'   => 'private',
			  'post_author'   => 1,
			);

			// Insert the post into the database
			$post_id = wp_insert_post( $my_post );
			
			update_field('field_545e4adb23773', strip_tags(mysql_real_escape_string($_POST['stripeEmail'])), $post_id); // Email
			update_field('field_545e4ae5d6266', '$' . strip_tags(mysql_real_escape_string($_POST['custom_donation'])), $post_id); // Amount
			update_field('field_545e4aecd6267', strip_tags(mysql_real_escape_string($_POST['honor'])), $post_id); // Honor Of
			update_field('field_545e4dfac41b4', $customer_id, $post_id); //Customer ID
	    }
	    else {
	      echo "<div class=\"error\">".$error."</div>";
	      //require_once('./payment_form.php');
	    }
	}
?>