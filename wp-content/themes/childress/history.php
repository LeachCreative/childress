<?php
	/*
		Template Name: History
	*/
	get_header();
	$id = get_queried_object_id();
?>
	<?php require_once('inc/page-title.php'); ?>
	<section class="list">
		<div class="container">
			<div class="row">
				<div class="col-md-10 col-md-push-1"><div class="divider"></div></div><div class="col-md-1"></div>
			</div>
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				


				<div class="flexible">
				<?php

					// check if the flexible content field has rows of data
					if( have_rows('flexible_content') ):

					     // loop through the rows of data
					    while ( have_rows('flexible_content') ) : the_row();
							echo '<div class="row">';
					        if( get_row_layout() == 'image_right_text_left' ):
					        	$img = get_sub_field('image');
					        	echo '<div class="col-md-6 col-md-push-1">
									' . get_sub_field('content') . '
								</div>
								<div class="col-md-4 col-md-push-1">
									<img src="' . $img['url'] . '">
								</div>';

					        elseif( get_row_layout() == 'image_left_text_right' ): 
					        	$img = get_sub_field('image');
					        	echo '<div class="col-md-4 col-md-push-1">
										<img src="' . $img['url'] . '">
									</div>
					        		<div class="col-md-6 col-md-push-1">
											' . get_sub_field('content') . '
									</div>';

					        endif;
					        
					        echo '</div>'; // Row
					        echo '<div class="row"><div class="col-md-10 col-md-push-1"><div class="divider"></div></div><div class="col-md-1"></div></div>';		
					    endwhile;

					else :

					    // no layouts found

					endif;

				?>
				</div><?php // Flexible ?>
				<?php endwhile; else : ?>
					<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
				<?php endif; ?>



				<section id="cd-timeline" class="cd-container">
				<div class="row">
				<div class="col-md-10 col-md-push-1">
				<?php 

					$args = array(
						'post_type' => 'history',
						'posts_per_page' => -1,
						'meta_key'		=> 'date',
						'orderby'		=> 'meta_value_num',
					);
					$custom_query = new WP_Query($args);
					while($custom_query->have_posts()) : $custom_query->the_post();
						echo '<div class="cd-timeline-block">
								<div class="cd-timeline-img cd-picture">
								</div> <!-- cd-timeline-img -->

								<div class="cd-timeline-content">
									<p>' . get_the_title() . '</p>' . get_the_content() . '
									<span class="cd-date">' . get_field('date') .'</span>
								</div> <!-- cd-timeline-content -->
							</div> <!-- cd-timeline-block -->';
					endwhile;
					wp_reset_postdata(); // reset the query 
				?>
				</div>
				</div><!-- Row -->
				</section> <!-- cd-timeline -->




		</div>
	</section>
				
	
			

			
		

	
	<?php get_template_part('mailing'); ?>

<?php get_footer(); ?>
<script  src="<?php bloginfo('template_directory'); ?>/js/timeline.js" type="text/javascript"></script>