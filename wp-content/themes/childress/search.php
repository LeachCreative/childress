<?php
	/*
	Template Name: Search Page
	*/
?>
<?php get_header(); ?>

	<?php require_once('inc/page-title.php'); ?>
	<section class="list">
		<div class="container">
			<?php 
			global $query_string;

			$query_args = explode("&", $query_string);
			$search_query = array();

			foreach($query_args as $key => $string) {
				$query_split = explode("=", $string);
				$search_query[$query_split[0]] = urldecode($query_split[1]);
			} // foreach

			$search = new WP_Query($search_query);
			while($search->have_posts()) : $search->the_post();  ?>
				<div class="row">
					<div class="col-md-10 col-md-push-1"><div class="divider"></div></div><div class="col-md-1"></div>
					<div class="col-md-10 col-md-push-1 content">
						<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						<?php the_excerpt(); ?><a class="readmore" href="<?php the_permalink(); ?>">Read More. ></a>
					</div>
				</div>
			<?php 
				endwhile;
				wp_reset_postdata(); // reset the query
			?>
			<div class="pagination">
				<?php echo paginate_links( $args ); ?>
			</div>
		</div>
	</section>
	
	<?php get_template_part('mailing'); ?>

<?php get_footer(); ?>
<?php get_header(); ?>

	<?php require_once('inc/page-title.php'); ?>
	<section class="list">
		<div class="container">
			<?php 
			global $query_string;

			$query_args = explode("&", $query_string);
			$search_query = array();

			foreach($query_args as $key => $string) {
				$query_split = explode("=", $string);
				$search_query[$query_split[0]] = urldecode($query_split[1]);
			} // foreach

			$search = new WP_Query($search_query);
			while($search->have_posts()) : $search->the_post();  ?>
				<div class="row">
					<div class="col-md-10 col-md-push-1"><div class="divider"></div></div><div class="col-md-1"></div>
					<div class="col-md-10 col-md-push-1 content">
						<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						<?php the_excerpt(); ?><a class="readmore" href="<?php the_permalink(); ?>">Read More. ></a>
					</div>
				</div>
			<?php 
				endwhile;
				wp_reset_postdata(); // reset the query
			?>
			<div class="pagination">
				<?php echo paginate_links( $args ); ?>
			</div>
		</div>
	</section>
	
	<?php get_template_part('mailing'); ?>

<?php get_footer(); ?>